output "route_table_id" {
  description = "List of IDs of public route tables"
  value       = aws_route_table.this.id
}
