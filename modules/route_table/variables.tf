variable "name" {
  description = "(Optional) The name of the route table."
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "(Required) The VPC ID."
  type        = string
  default     = null
}

variable "subnet_id" {
  description = "(Optional) The subnet ID to create an association. "
  type        = string
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A map of tags to assign to the resource."
  default     = {}
}
