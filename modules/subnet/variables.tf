variable "name" {
  description = "(Optional) The name of the subnet."
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "(Optional) The id of the VPC that the desired subnet belongs to."
  type        = string
  default     = null
}

variable "cidr_block" {
  description = "(Optional) The cidr block of the desired subnet."
  type        = string
  default     = null
}

variable "availability_zone" {
  description = "(Optional) The availability zone where the subnet must reside."
  type        = string
  default     = null
}

variable "availability_zone_id" {
  description = "(Optional) The ID of the Availability Zone for the subnet."
  type        = string
  default     = null
}

variable "assign_ipv6_address_on_creation" {
  description = "(Optional, Forces new resource) The name of the bucket. If omitted, Terraform will assign a random, unique name."
  type        = string
  default     = null
}

variable "ipv6_cidr_block" {
  description = "(Optional) The Ipv6 cidr block of the desired subnet"
  type        = string
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A map of tags, each pair of which must exactly match a pair on the desired subnet."
  default     = {}
}
