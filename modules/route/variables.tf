variable "route_table_id" {
  description = "(Required) The ID of the routing table."
  type        = string
  default     = null
}

variable "destination_cidr_block" {
  description = "(Optional) The destination CIDR block."
  type        = string
  default     = null
}

variable "destination_ipv6_cidr_block" {
  description = "(Optional) The destination IPv6 CIDR block."
  type        = string
  default     = null
}

variable "egress_only_gateway_id" {
  description = "(Optional) Identifier of a VPC Egress Only Internet Gateway."
  type        = string
  default     = null
}

variable "gateway_id" {
  description = "(Optional) Identifier of a VPC internet gateway or a virtual private gateway."
  type        = string
  default     = null
}

variable "instance_id" {
  description = "(Optional) Identifier of an EC2 instance."
  type        = string
  default     = null
}

variable "nat_gateway_id" {
  description = "(Optional) Identifier of a VPC NAT gateway."
  type        = string
  default     = null
}

variable "local_gateway_id" {
  description = "(Optional) Identifier of a Outpost local gateway."
  type        = string
  default     = null
}

variable "network_interface_id" {
  description = "(Optional) Identifier of an EC2 network interface."
  type        = string
  default     = null
}

variable "transit_gateway_id" {
  description = "(Optional) Identifier of an EC2 Transit Gateway."
  type        = string
  default     = null
}

variable "vpc_peering_connection_id" {
  description = "(Optional) Identifier of a VPC peering connection."
  type        = string
  default     = null
}
