variable "connectivity_type" {
  description = "(Optional) Connectivity type for the gateway. Valid values are private and public. Defaults to public."
  type        = string
  default     = null
}

variable "subnet_id" {
  description = "(Required) The Subnet ID of the subnet in which to place the gateway."
  type        = string
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A map of tags to assign to the resource."
  default     = {}
}
