resource "aws_eip" "this" {
  vpc  = true
  tags = var.tags
}

resource "aws_nat_gateway" "this" {
  allocation_id     = aws_eip.this.id
  connectivity_type = var.connectivity_type
  subnet_id         = var.subnet_id
  tags              = var.tags
}
